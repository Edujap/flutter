import 'package:flutter/material.dart';
import 'package:untitled/menu.dart';

import 'juego2.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: _login(),);
  }
}
class _login extends StatefulWidget {

  @override
  State<_login> createState() => _loginState();
}

class _loginState extends State<_login> {
  TextEditingController email= new TextEditingController();
  TextEditingController password= new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: Container(
        decoration: BoxDecoration( 
          color: Colors.blue[50],
          borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.black38,
                blurRadius: 25.0,
                spreadRadius: 5.0,
                offset: Offset(15.0,15.0),
              )
            ]
        ),
        margin:EdgeInsets.only(top:50,left:20,right: 20,bottom: 50),
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Center(
          child:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset("assets/ui_amoung.png",height: 150,),
              Text(
                'PickUp',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                  fontFamily: "Hide" ,
                ),
              ),
              SizedBox(height: 50),
              Container(
                margin: EdgeInsets.only(top:30),
                width: 400,
                height: 150,
                child:Column(
                  children: <Widget>[
                    Expanded(
                    child:FlatButton(
                      color:Colors.grey,
                      minWidth: 150,
                      child: Text("Menu",style: TextStyle(color:Colors.white, fontSize: 20),),
                      onPressed: (){Navigator.push(context,MaterialPageRoute(builder: (_)=>pantallaMenu()));})),
                    SizedBox(height: 16),
                    Expanded(
                      child:FlatButton(
                          color:Colors.grey,
                          minWidth: 150,
                          child: Text("Juego 1",style: TextStyle(color:Colors.white, fontSize: 20),),
                          onPressed: (){Navigator.push(context,MaterialPageRoute(builder: (_)=>pantallaMenu()));})) ,
                    SizedBox(height: 16),
                    Expanded(
                        child:FlatButton(
                            color:Colors.grey,
                            minWidth: 150,
                            child: Text("Juego 2",style: TextStyle(color:Colors.white, fontSize: 20),),
                            onPressed: (){Navigator.push(context,MaterialPageRoute(builder: (_)=>pantallaJuego2()));})
                    ),

                  ],
                ),
                ),



              SizedBox(height: 100),

              Text("problemas legales y otros con Diego Grell Casaverde arequipa,Peru",
                  style: TextStyle(color:Colors.grey[900], fontSize:7))
            ],
          ),
        ),
      ),
    );
  }
}


class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: ()  { Navigator.push(context,MaterialPageRoute(builder: (_)=>pantallaMenu()));
          },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
